# Replicability of the Experimentation

## Items to Replicate
* Figures from table VI, VII, VIII
* t-statistics and p-values for experiments
    1. Proposed model vs Baseline model 
    1. Proposed model at 15% of train data vs Baseline at 100% data
    1. Proposed model simultaneous transfer learning vs Sequential Transfer learning
* Weights for baseline model and our model
* Risk factors for a specific model

# Installation
## 0. Prerequisites
1. Python 3.7.x
1. A GPU of 4Go and Cuda 10.1
1. Git large file system extension (git-lfs)
    * https://packagecloud.io/github/git-lfs/install#bash-deb 
    * ubuntu: `sudo apt install git-lfs`
1. A copy of the repository (This includes code and datasets)
    * `https://gitlab.com/anon2020/dsaa2020.git`
1. SpaCy English word vector model (large)
1. NodeJS, NPM and NeAt-Vision for attention weights visualization
   * https://github.com/cbaziotis/neat-vision

## 1 Installation Procedure
1. `sudo apt install git-lfs`
2. `git lsf install`
3. `git clone https://gitlab.com/anon2020/dsaa2020.git`
4. `cd dsaa2020`
5. `python3 -m venv .venv`
6. `source .venv/bin/activate`
7. `pip install -r requirements`
8. `spacy download en_core_web_lg`

## Manual Replication

## 0 Launching Everything at Once
1. `sh run_paper.sh` 
 

## 1. Figure from Tables & t-statistics
Run the command ``` python3 generate_officiel_figures.py ``` 

Figures for tables VI, VII and VIII will be presented in the output

     
## 2. Training Process
### Train Models
Run the command: ``` python3 train_model.py ```

Use the following arguments in the CLI
* `-m <model>`:  use `HAN` for baseline model and `MLHAN` for proposed model
* `-r <seed>`: for a specific random starting point (default is `42`). 
* `-e <experiment-name>`: use `fullcorpus` for full experiment, `multilabel` for multi-label task on lower-level, `10pct`, `15pct`,`30pct`, for generability experiment at 10%, 15% and 30% of the train dataset, `xlearn` for transfer learning experiment 

Examples:
- `python3 train.py -m han -e fullcorpus -r 1337`
- `python3 train.py -m mlhan -e fullcorpus -r 1337`
- `python3 train.py -m han -e 15pct -r 1203`
- `python3 train.py -m mlhan -e 15pct -r 1203`
- `python3 train.py -m mlhan -e xlearn -r 1234`

After the training process, attention weights will be produced.

### Evaluate Models
Run the command: ```python3 evaluate_models.py```

It will recursively evaluate all trained model and present the performance in the output

## 3. Obtain Risk Factors
Run the command: ```python3 get_risk_factors.py```

Use the same arguments used to retrieve the trained the model 
* `-m <model>`:  `HAN` or `MLHAN` 
* `-r <seed>`: the random starting point used
* `-e <experiment-name>`: use `fullcorpus`, `10pct`, `15pct`,`30pct` or `xlearn`  

Risk factor per intoxicant will be presented in the output

## 4. Visualizing Attention Weights
Launch NeAt-Vision `npm start`

Use the attention weights generated from the Training Process.
