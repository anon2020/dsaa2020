pip install -r requirements.txt

python -u -m train_model -m han -e fullcorpus -r 1337
python -u -m train_model -m han -e fullcorpus -r 420
python -u -m train_model -m han -e fullcorpus -r 26

python -u -m train_model -m mlhan -e fullcorpus -r 1337
python -u -m train_model -m mlhan -e fullcorpus -r 420
python -u -m train_model -m mlhan -e fullcorpus -r 26

python -u -m train_model -m han -e 15pct -r 1337
python -u -m train_model -m mlhan -e 15pct -r 1337

python -u -m train_model -m mlhan -e multilabel -r 1337

python -u -m evaluate_models

python -u -m get_risk_factors -m mlhan -e multilabel -r 1337
